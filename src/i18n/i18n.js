import {addMessages, getLocaleFromNavigator, init} from 'svelte-i18n';
import en from '../i18n/en.json';
import fr from '../i18n/fr.json';
import ar from '../i18n/ar.json';

addMessages('en', en);
addMessages('fr', fr);
addMessages('ar', ar);

init({
    fallbackLocale: 'fr',
    initialLocale: getLocaleFromNavigator(),
});